require icu.inc

LIC_FILES_CHKSUM = "file://../license.html;md5=c7c2fb5f3b811a12e9621020793cf999"

PR = "r2"

BASE_SRC_URI = "http://download.icu-project.org/files/icu4c/4.8.1.1/icu4c-4_8_1_1-src.tgz"
SRC_URI = "${BASE_SRC_URI}"

SRC_URI[md5sum] = "ea93970a0275be6b42f56953cd332c17"
SRC_URI[sha256sum] = "0a70491c5fdfc5a0fa7429f820da73951e07d59a268b3d8ffe052eec65820ca1"
