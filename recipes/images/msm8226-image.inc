# The mkbootimg production process.
require msm-image.inc

# Force a few additional dependencies in the mix so that we get the needed
# recipes to build in the right order so we can make the bootimg file and
# our yaffs2 image...
DEPENDS = 	" \
		virtual/kernel \
		virtual/armel-bootloader \
		gettext-native \
		mkbootimg-native \
		dtbtool-native \
		"

# Image output types
IMAGE_FSTYPES = "ext4"

# The OE-Core minimal boot image files.
IMAGE_INSTALL += "task-core-boot"
IMAGE_INSTALL += "${ROOTFS_PKGMANAGE_BOOTSTRAP}"
IMAGE_INSTALL += "${POKY_EXTRA_INSTALL}"

# The MSM Linux minimal boot image files.
IMAGE_INSTALL += "kernel-modules"
IMAGE_INSTALL += "base-files"
IMAGE_INSTALL += "base-passwd"
IMAGE_INSTALL += "powerapp"
IMAGE_INSTALL += "powerapp-powerconfig"
IMAGE_INSTALL += "powerapp-reboot"
IMAGE_INSTALL += "powerapp-shutdown"
IMAGE_INSTALL += "reboot-daemon"
IMAGE_INSTALL += "system-core-adbd"
IMAGE_INSTALL += "system-core-usb"
IMAGE_INSTALL += "system-core-liblog"
IMAGE_INSTALL += "system-core-libcutils"
#IMAGE_INSTALL += "start-scripts-backlight"
IMAGE_INSTALL += "start-scripts-firmware-links"
IMAGE_INSTALL += "alsa-utils-amixer"
IMAGE_INSTALL += "alsa-utils-aplay"
IMAGE_INSTALL += "alsa-utils-speakertest"
IMAGE_INSTALL += "alsa-utils-midi"
IMAGE_INSTALL += "alsa-intf"
IMAGE_INSTALL += "udev"
IMAGE_INSTALL += "libstdc++"
IMAGE_INSTALL += "mm-video-oss"
IMAGE_INSTALL += "mm-core-oss"
IMAGE_INSTALL += "camera-hal"
IMAGE_INSTALL += "open-source-kernel-tests"
IMAGE_INSTALL += "mm-image-codec"
IMAGE_INSTALL += "init-audio"
IMAGE_INSTALL += "wcnss"
IMAGE_INSTALL += "wireless-tools"
IMAGE_INSTALL += "dhcp-client"
IMAGE_INSTALL += "wlan-opensource"

# Convenience: build a compatible GDB and install gdbserver on the image
DEPENDS += "gdb-cross"
IMAGE_INSTALL += "gdbserver"
IMAGE_INSTALL += "strace"
IMAGE_INSTALL += "loc-api"
#IMAGE_INSTALL += "msm7k"

IMAGE_PREPROCESS_COMMAND += "mv ${IMAGE_ROOTFS}/lib/modules ${IMAGE_ROOTFS}/usr/lib;"

IMAGE_PREPROCESS_COMMAND += "mkdir -p ${IMAGE_ROOTFS}/cache;"
IMAGE_PREPROCESS_COMMAND += "echo ro.build.version.release=`cat ${IMAGE_ROOTFS}/etc/version ` >> ${IMAGE_ROOTFS}/build.prop;"

# Make the bootimg image file using the information available in the sysroot...
do_rootfs_append() {
	# Make bootimage
	ver=`sed -r 's/#define UTS_RELEASE "(.*)"/\1/' ${STAGING_KERNEL_DIR}/include/generated/utsrelease.h`

	dtb_files=`find ${STAGING_KERNEL_DIR}/arch/arm/boot/dts -iname *.dtb | awk -F/ '{print $NF}' | awk -F[.][d] '{print $1}'`

	# Create separate images with dtb appended to zImage for all targets.
	for d in ${dtb_files}; do
		targets=`echo ${d#-}`
		cat ${STAGING_DIR_TARGET}/boot/zImage-${ver} ${STAGING_KERNEL_DIR}/arch/arm/boot/dts/${d}.dtb > ${STAGING_KERNEL_DIR}/arch/arm/boot/dtb-zImage-${ver}-${targets}
	done

	${STAGING_BINDIR_NATIVE}/dtbtool -o ${STAGING_DIR_TARGET}/boot/dt.img -p ${STAGING_KERNEL_DIR}/scripts/dtc/ -v ${STAGING_KERNEL_DIR}/arch/arm/boot/dts/
	chmod a+r ${STAGING_DIR_TARGET}/boot/dt.img

	# Updated base address according to new memory map.
	${STAGING_BINDIR_NATIVE}/mkbootimg --kernel ${STAGING_DIR_TARGET}/boot/zImage-${ver} \
        --dt ${STAGING_DIR_TARGET}/boot/dt.img \
		--ramdisk /dev/null \
		--cmdline "noinitrd root=${MACHINE_ROOTDEV} rw init=/sbin/init --verbose loglevel=7 console=${MACHINE_CONSOLE},115200,n8 earlyprintk androidboot.console=${MACHINE_CONSOLE} androidboot.hardware=qcom mem=512M@0x00000000" \
		--base ${MACHINE_KERNEL_BASE} \
		--tags-addr ${MACHINE_KERNEL_TAGS_OFFSET} \
		--output ${DEPLOY_DIR_IMAGE}/${PN}-boot-${MACHINE}.img

        cp ${DEPLOY_DIR_IMAGE}/${PN}-boot-${MACHINE}.img ${DEPLOY_DIR_IMAGE}/${PN}-boot-${MACHINE}.img.nonsecure
        ${STAGING_BINDIR_NATIVE}/openssl dgst -${TARGET_SHA_TYPE} -binary ${DEPLOY_DIR_IMAGE}/${PN}-boot-${MACHINE}.img.nonsecure > ${DEPLOY_DIR_IMAGE}/${PN}-boot-${MACHINE}.img.${TARGET_SHA_TYPE}
        ${STAGING_BINDIR_NATIVE}/openssl rsautl -sign -in ${DEPLOY_DIR_IMAGE}/${PN}-boot-${MACHINE}.img.${TARGET_SHA_TYPE} -inkey ${WORKSPACE}/${PRODUCT_PRIVATE_KEY} -out ${DEPLOY_DIR_IMAGE}/${PN}-boot-${MACHINE}.img.sig
        dd if=/dev/zero of=${DEPLOY_DIR_IMAGE}/${PN}-boot-${MACHINE}.img.sig.padded bs=${MACHINE_FLASH_PAGE_SIZE} count=1
        dd if=${DEPLOY_DIR_IMAGE}/${PN}-boot-${MACHINE}.img.sig of=${DEPLOY_DIR_IMAGE}/${PN}-boot-${MACHINE}.img.sig.padded conv=notrunc
        cat ${DEPLOY_DIR_IMAGE}/${PN}-boot-${MACHINE}.img.nonsecure ${DEPLOY_DIR_IMAGE}/${PN}-boot-${MACHINE}.img.sig.padded > ${DEPLOY_DIR_IMAGE}/${PN}-boot-${MACHINE}.img
        rm -rf ${DEPLOY_DIR_IMAGE}/${PN}-boot-${MACHINE}.img.nonsecure ${DEPLOY_DIR_IMAGE}/${PN}-boot-${MACHINE}.img.${TARGET_SHA_TYPE} ${DEPLOY_DIR_IMAGE}/${PN}-boot-${MACHINE}.img.sig ${DEPLOY_DIR_IMAGE}/${PN}-boot-${MACHINE}.img.sig.padded

}

